const database = require('mongoose');
const { makeLogger } = require('jrz-logger');

const logger = makeLogger(process.pid, process.env.LOG_LEVEL);

database.connection.on('connected', () => { logger.info('MONGO Connected'); });
database.connection.on('disconnected', () => { logger.warn('MONGO Disconnected'); });

const options = {
  bufferCommands: true,
  poolSize: 2,
  promiseLibrary: Promise,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

database.set('debug', process.env.NODE_ENV === 'Development');
database.set('useFindAndModify', false);
database.set('useCreateIndex', true);

module.exports = (connectionString) => {
  if (!connectionString) return database;
  return database
    .connect(connectionString, options)
    .catch(() => { logger.error('Database connection failed'); });
};
