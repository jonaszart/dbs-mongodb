const database = require('mongoose');

const schema = new database.Schema({
	title: { type: String, required: true },
  price: { type: Number, required: true },
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt'
  }
});

schema.index({
  title: 1
});

module.exports = database.model('Products', schema);
