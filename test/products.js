const assert = require('assert');
const { randomProductName, randomNumber } = require('jrz-helpers');
const {
	connect,
	Products
} = require('../index');

describe('', async () => {
	connect('mongodb://localhost:2727/dbs');
  it('should insert Product', async () => {
    const data = {
			title: randomProductName(),
			price: randomNumber(111, 999)
    };
    const insert = await Products.create(data);
    assert.equal(true, typeof insert._id !== 'undefined');
  });
});
